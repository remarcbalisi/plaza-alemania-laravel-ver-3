<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRoomTypeIdGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room_gallery', function (Blueprint $table) {
            $table->dropForeign('room_gallery_room_type_id_foreign');
            $table->dropColumn('room_type_id');
            // $table->integer('room_type_id')->unsigned()->index()->nullable();
            // $table->foreign('room_type_id')->references('id')->on('room_type')->onDelete('cascade');
        });
    }

}
