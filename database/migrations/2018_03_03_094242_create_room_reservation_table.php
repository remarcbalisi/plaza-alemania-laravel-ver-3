<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_reservation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->unsigned()->index()->nullable();
            $table->foreign('room_id')->references('id')->on('room')->onDelete('cascade');
            $table->integer('reservation_id')->unsigned()->index()->nullable();
            $table->foreign('reservation_id')->references('id')->on('reservation')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_reservation');
    }
}
