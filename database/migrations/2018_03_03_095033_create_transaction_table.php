<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->float('total_reservation_amt')->default(0);
            $table->float('total_in_room_order')->default(0);
            $table->float('downpayment')->default(0);
            $table->float('grand_total')->default(0);
            $table->boolean('is_cancelled')->default(false);
            $table->boolean('is_completed')->default(false);
            $table->integer('discount_id')->unsigned()->index()->nullable();
            $table->foreign('discount_id')->references('id')->on('discount');
            $table->integer('confirmed_by')->unsigned()->index()->nullable();
            $table->foreign('confirmed_by')->references('id')->on('users');
            $table->integer('branch_id')->unsigned()->index()->nullable();
            $table->foreign('branch_id')->references('id')->on('branch');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
